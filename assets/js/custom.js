	function myFunction() {
        var icon = document.getElementById("top_nav");
        if (icon.style.display === "block") {
            icon.style.display = "none";
            } else {
            icon.style.display = "block";
        }
    }
    function showData(evt, languageCode){
        var i, content, button;
        content = document.getElementsByClassName("endpointContent");
        for (i=0; i<content.length; i++){
            content[i].style.display="none";
        }
        button = document.getElementsByClassName("code_buttons");
        for(i=0; i<button.length; i++){
            button[i].className = button[i].className.replace("active","");
        }
        document.getElementById(languageCode).style.display = "block";
        evt.currentTarget.className += " active";
    }
    /*For changing values in pricing planes*/
    function changeValues(e){
        var monthlyOryearly = document.getElementsByClassName("perMonthorYear");
        var starter = document.getElementById("starter");
        var growth =  document.getElementById("growth");
        var professional = document.getElementById("professional");     
        var start = document.getElementById("starterLi");
        var grow =  document.getElementById("growthLI");
        var prof = document.getElementById("professionalLi");

        if(e.checked){
            start.innerHTML = "180K API Calls/Year";
            grow.innerHTML = "1.8M API Calls/Year";
            prof.innerHTML = "6.6M API Calls/Year";
            starter.innerHTML = "US$99.99";
            growth.innerHTML = "US$499.99";
            professional.innerHTML = "US$999.99";
            for (var i=0; i < monthlyOryearly.length; i++) {
                monthlyOryearly[i].innerHTML = "/Year";
            }
        }else{
            starter.innerHTML = "US$9.99";
            growth.innerHTML = "US$49.99";
            professional.innerHTML = "US$99.99";
            start.innerHTML = "15K API Calls/Month";
            grow.innerHTML = "150K API Calls/Month";
            prof.innerHTML = "550K API Calls/Month";
            for (var i=0; i < monthlyOryearly.length; i++) {
                monthlyOryearly[i].innerHTML = "/Month";
            }
        }        
    }
    function myTable(e){
        var gbpValue = document.getElementById("gbp");
        var usdValue = document.getElementById("usd");
        var eurValue = document.getElementById("eur");
        var pkrValue = document.getElementById("onepkr");
        var gbp_Data = document.getElementById("gbpData");
        var usd_Data = document.getElementById("usDollar");
        var eur_Data = document.getElementById("eurData");
        if(e.checked){
            gbpValue.innerHTML = "1 GBP";
            usdValue.innerHTML = "1 USD";
            eurValue.innerHTML = "1 EUR";
            pkrValue.innerHTML = "PKR";
            gbp_Data.innerHTML = "218.4516";
            usd_Data.innerHTML = "160.3450";
            eur_Data.innerHTML = "196.6825";
        }else{
            gbpValue.innerHTML = "GBP";
            usdValue.innerHTML = "USD";
            eurValue.innerHTML = "EUR";
            pkrValue.innerHTML = "1 PKR";
            gbp_Data.innerHTML = "0.0046";
            usd_Data.innerHTML = "0.0062";
            eur_Data.innerHTML = "0.0051";
        }
    }
    function swapValues(){
        var fromCur = document.getElementById("fromCurrency").value;
        var toCur = document.getElementById("toCurrency").value;
        document.getElementById("fromCurrency").value = document.getElementById("toCurrency").value;
        document.getElementById("toCurrency").value = fromCur;
    }
    function showList(x){
        x = document.getElementById("fromCurrency").value;
        x.style.display = "none";
    }